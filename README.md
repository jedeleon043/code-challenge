# Code Challenge
## Runtime Prerequisites
- The api was build using spring boot leveraging a java 10.0.2 runtime. However, there shouldn't be many dependencies on java 10.0.2 APIs. At a minimum Java 8 should be installed.
## DB Prerequisites
- Start docker based couchbase container
    - ```docker run -d --name db -p 8091-8094:8091-8094 -p 11210:11210 couchbase:community-4.5.1```
- Configure bucket with name gifs and a password
- Override spring application config by setting environment variable or by setting them directly in the application.yaml file.
    - ```export SPRING_COUCHBASE_BUCKET_PASSWORD=<the password you configured>```
## Security
- The API is secured by leveraging Auth0 as an identity provider while including spring security dependencies and configurations.
- In order to run the API service locally, you must have an Auth0 account configured to allow redirects back to localhost:8080.
- You must also obtain the client id from Auth0 and either override spring external configs via an environment variable or set the property directly in the application.yaml
## Running the API Locally
```./gradlew bootRun```

## Giphy Search Resource
   - ```curl --header "Authorization: <access token>" localhost:8080/v1/api/giphy/search?query=<gifs to search>```
   
   
## Categories Resource
### Get Categories
   - ```curl --header "Authorization: <access token>" localhost:8080/v1/api/categories```
### Create Category
   - ```curl --header "Authorization: <access token>" -d '{"categoryName":"funny"}' -H "Content-Type: application/json" -X POST localhost:8080/v1/api/categories```

   
## Gifs Resource
### Recategorize a GIF
   - ```curl --header "Authorization: <access token>" -d '{"categoryName":"funny", "currentCategoryName":"hilarious"}' -H "Content-Type: application/json" -X POST localhost:8080/v1/api/gif?id=<giphy id/url>```
   
## Notes
This is API is only for testing purposes. It should in no way be considered a production ready service.