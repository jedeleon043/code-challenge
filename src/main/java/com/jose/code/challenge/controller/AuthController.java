package com.jose.code.challenge.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@RestController
public class AuthController {

    @Value("${auth0.authendpoint}")
    private String authEndpoint;


    @Value("${auth0.audience}")
    private String audience;

    @Value("${auth0.issuer}")
    private String issuer;


    @Value("${auth0.client}")
    private String client;


    @Value("${server.servlet.context-path}")
    private String basePath;

    @GetMapping(path = "/login")
    public void login(HttpServletResponse response, HttpServletRequest request, HttpSession session) throws IOException {
        String state = UUID.randomUUID().toString();
        StringBuilder builder = new StringBuilder();
        session.setAttribute("state", state);
        builder.append(authEndpoint)
                .append("?audience=")
                .append(audience)
                .append("&scope=")
                .append("write:categories%20read:categories%20read:gifs")
                .append("&response_type=token")
                .append("&client_id=")
                .append(client)
                .append("&redirect_uri=")
                .append(request.getScheme() + "://" + request.getServerName() + ":3000/authorize")
                .append("&state=")
                .append(state);

        response.sendRedirect(builder.toString());
    }

    // TODO access token retrieval callback will be on front end since we are using implicit grant.
}
