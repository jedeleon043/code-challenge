package com.jose.code.challenge.controller;

import com.jose.code.challenge.client.ro.GiphyResponseRO;
import com.jose.code.challenge.ro.exception.GiphyApiException;
import com.jose.code.challenge.ro.exception.GiphyApiParsingException;
import com.jose.code.challenge.service.GiphyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;

@RestController
@Validated
@RequestMapping(path="/giphy")
public class GIFSearchController {

    @Autowired
    GiphyService giphyService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/search")
    public ResponseEntity findGif(
        @RequestParam(value="query")
        // only alpha and url encoded whitespace characters up to 50 allowed
        @Pattern(regexp = "^(\\w|\\s){1,50}") String query
    ) throws GiphyApiException, GiphyApiParsingException {
        GiphyResponseRO giphyResponseRO = giphyService.queryGiphy(query);
        return  ResponseEntity.ok(giphyResponseRO);
    }
}
