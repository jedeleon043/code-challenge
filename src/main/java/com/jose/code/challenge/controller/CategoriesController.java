package com.jose.code.challenge.controller;

import com.jose.code.challenge.data.ro.GifCategory;
import com.jose.code.challenge.ro.CreateCategoryRO;
import com.jose.code.challenge.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

import static com.jose.code.challenge.utils.ConstraintPatterns.*;


@RestController
@Validated
@RequestMapping(path="/categories")
public class CategoriesController {

    @Autowired
    CategoryService categoryService;

    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity getCategories(Authentication authentication) {
        String subject = authentication.getName();
        List<GifCategory> categories = categoryService.getCategories(subject);
        return  ResponseEntity.ok(categories);
    }

    @PostMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity addCategory(
            @Valid @RequestBody CreateCategoryRO catagoryRequest,
            Authentication authentication) {


        String subject = authentication.getName();
        categoryService.saveNewCategory(catagoryRequest.getCategoryName(), subject);
        return  ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity deleteGif(
            @PathVariable(name = "id", required = true)
            @Pattern(regexp = CATEGORY_NAME_PATTERN) String categoryName,
            Authentication authentication) {
        String subject = authentication.getName();
        categoryService.deleteCategory(categoryName, subject);
        return  ResponseEntity.ok().build();
    }
}
