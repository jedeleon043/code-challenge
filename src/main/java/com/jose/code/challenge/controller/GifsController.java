package com.jose.code.challenge.controller;

import com.jose.code.challenge.ro.SaveGifRO;
import com.jose.code.challenge.ro.UpdateGifCategoryRO;
import com.jose.code.challenge.service.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import static com.jose.code.challenge.utils.ConstraintPatterns.CATEGORY_NAME_PATTERN;
import static com.jose.code.challenge.utils.ConstraintPatterns.GIPHY_ID_PATTERN;

@RestController
@Validated
@RequestMapping(path="/gif")
public class GifsController {


    @Autowired
    GifService gifService;

    @PostMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity addGif(@RequestBody  SaveGifRO gifRequest, Authentication authentication) {


        String subject = authentication.getName();
        gifService.saveGif(gifRequest.getGiphyIds(), gifRequest.getCategoryName(), subject);
        return ResponseEntity.ok().build();
    }

    @PutMapping()
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity updateGif(
            @RequestParam(name = "id", required = true)
            @Pattern(regexp = GIPHY_ID_PATTERN) String giphyId,
            @Valid @RequestBody
            UpdateGifCategoryRO gifRequest,
            Authentication authentication) {

        String subject = authentication.getName();
        gifService.updateGifCategory(subject, giphyId, gifRequest.getCategoryName(), gifRequest.getCurrentCategoryName());
        return  ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity deleteGif(
            @PathVariable(name = "id", required = true)
            @Pattern(regexp = GIPHY_ID_PATTERN) String giphyId,
            @RequestParam(value = "category")
            @Pattern(regexp = CATEGORY_NAME_PATTERN)String categoryName,
            Authentication authentication){

        String subject = authentication.getName();
        gifService.deleteGif(subject, giphyId, categoryName);
        return  ResponseEntity.ok().build();
    }
}
