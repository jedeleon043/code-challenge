package com.jose.code.challenge.controller.exception;

import com.jose.code.challenge.ro.ErrorEntityRO;
import com.jose.code.challenge.ro.ErrorStatus;
import com.jose.code.challenge.ro.exception.GiphyApiException;
import com.jose.code.challenge.ro.exception.GiphyApiParsingException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ExceptionMapper extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleContraintViolations(ConstraintViolationException e, WebRequest request) {
        ErrorEntityRO error = new ErrorEntityRO(
                ErrorStatus.CONTRAINT_VIOLATION.ordinal(),
                ErrorStatus.CONTRAINT_VIOLATION.getMessage()
        );
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(GiphyApiException.class)
    public ResponseEntity handleGiphyApiViolations(GiphyApiException e, WebRequest request) {
        ErrorEntityRO error = new ErrorEntityRO(
                ErrorStatus.GIPHY_API_EXCEPTION.ordinal(),
                ErrorStatus.GIPHY_API_EXCEPTION.getMessage()
        );
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(GiphyApiParsingException.class)
    public ResponseEntity handleGiphyApiViolations(GiphyApiParsingException e, WebRequest request) {
        ErrorEntityRO error = new ErrorEntityRO(
                ErrorStatus.GIPHY_PARSE_EXCEPTION.ordinal(),
                ErrorStatus.GIPHY_PARSE_EXCEPTION.getMessage()
        );
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorEntityRO error = new ErrorEntityRO(
                ErrorStatus.CONTRAINT_VIOLATION.ordinal(),
                ErrorStatus.CONTRAINT_VIOLATION.getMessage()
        );
        return ResponseEntity.badRequest().body(error);
    }
}
