package com.jose.code.challenge.utils;

public class ConstraintPatterns {
    public static final String CATEGORY_NAME_PATTERN = "^([A-Z]|[a-z]){1,20}$";
    public static final String GIPHY_ID_PATTERN = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";
}
