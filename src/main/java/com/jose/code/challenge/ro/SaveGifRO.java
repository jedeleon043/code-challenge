package com.jose.code.challenge.ro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.util.List;

import static com.jose.code.challenge.utils.ConstraintPatterns.CATEGORY_NAME_PATTERN;
import static com.jose.code.challenge.utils.ConstraintPatterns.GIPHY_ID_PATTERN;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveGifRO {

    // Can be null if user didnt specify category
    @Pattern(regexp = CATEGORY_NAME_PATTERN)
    private String categoryName;

    @NotNull
    @Pattern(regexp = GIPHY_ID_PATTERN)
    private List<String> giphyIds;
}
