package com.jose.code.challenge.ro.exception;

public class GiphyApiException extends Exception {
    public GiphyApiException() {
    }

    public GiphyApiException(String message) {
        super(message);
    }

    public GiphyApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public GiphyApiException(Throwable cause) {
        super(cause);
    }

    public GiphyApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
