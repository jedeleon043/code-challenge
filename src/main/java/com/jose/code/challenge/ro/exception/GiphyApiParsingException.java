package com.jose.code.challenge.ro.exception;

public class GiphyApiParsingException extends Exception {
    public GiphyApiParsingException() {
    }

    public GiphyApiParsingException(String message) {
        super(message);
    }

    public GiphyApiParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public GiphyApiParsingException(Throwable cause) {
        super(cause);
    }

    public GiphyApiParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
