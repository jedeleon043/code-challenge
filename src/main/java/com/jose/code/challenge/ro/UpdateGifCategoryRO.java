package com.jose.code.challenge.ro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.jose.code.challenge.utils.ConstraintPatterns.CATEGORY_NAME_PATTERN;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateGifCategoryRO {

    @NotNull
    @Pattern(regexp = CATEGORY_NAME_PATTERN)
    private String currentCategoryName;

    @NotNull
    @Pattern(regexp = CATEGORY_NAME_PATTERN)
    private String categoryName;
}
