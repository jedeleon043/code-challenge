package com.jose.code.challenge.ro;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.jose.code.challenge.utils.ConstraintPatterns.CATEGORY_NAME_PATTERN;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateCategoryRO {

    @NotNull(message = "categoryName must not be null")
    @Pattern(regexp = CATEGORY_NAME_PATTERN, message = "invalid category name format") // max 10 char word
    @JsonProperty(value = "categoryName")
    private String categoryName;
}
