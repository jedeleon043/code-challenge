package com.jose.code.challenge.ro;

public enum ErrorStatus {

    CONTRAINT_VIOLATION("Bad Request"),
    GIPHY_API_EXCEPTION("System Error"),
    GIPHY_PARSE_EXCEPTION("System Error");

    private String message;

    private ErrorStatus(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
