package com.jose.code.challenge.data.repositories;

import com.jose.code.challenge.data.ro.GifCategory;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbasePagingAndSortingRepository;

import java.util.List;

//@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "categories")
public interface CategoriesRepository extends CouchbasePagingAndSortingRepository <GifCategory, String> {

    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND categoryName = $1")
    GifCategory findByCategoryName(String categoryName);

    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND userId = $1")
    List<GifCategory> findByUserId(String userId);

    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND userId = $1 AND id = $2")
    GifCategory findByCategoryNameAndUserId(String categoryName, String userId);
}
