package com.jose.code.challenge.data.ro;


import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class GifCategory {
    @NotNull
    @Id
    private String categoryName; // PK

    @NotNull
    @Field
    private String userId; // FK

    @Field
    private List<GifObject> gifObjects;
}
