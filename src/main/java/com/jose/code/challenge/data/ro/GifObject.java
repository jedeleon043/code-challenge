package com.jose.code.challenge.data.ro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class GifObject {

    @NotNull
    private String giphyId;

    @NotNull
    private String categoryName;

    @NotNull
    private String userId;
}
