package com.jose.code.challenge.data;

import com.jose.code.challenge.data.repositories.CategoriesRepository;
import com.jose.code.challenge.data.ro.GifCategory;
import com.jose.code.challenge.data.ro.GifObject;
import com.jose.code.challenge.data.ro.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DAOFacade {

    @Autowired
    CategoriesRepository categoriesRepository;

    /*
        User CRUD operations
     */
    public User createUser(User user) {
        return null;
    }

    public User retrieveUser() {
        return null;
    }

    public User updateUser(User user) {
        return null;
    }

    public void deleteUser(User user) {

    }


    /*
        Category CRUD operations
     */
    public GifCategory createCategory(GifCategory category) {
        return categoriesRepository.save(category);
    }

    public List<GifCategory> retrieveCategories(String userId) {
        List<GifCategory> categories = categoriesRepository.findByUserId(userId);
        return categories;
    }

    public GifCategory retrieveCategory(String userId, String categoryName) {
        Optional<GifCategory> optional = categoriesRepository.findById(categoryName);

        if(optional.isPresent()) {
            return optional.filter(category -> category.getUserId().equals(userId)).get();
        }
        return null;
    }

    public GifCategory updateCategory(GifCategory category) {
        categoriesRepository.save(category);
        return null;
    }

    public void deleteCategory(String categoryName) {
        categoriesRepository.deleteById(categoryName);
    }

    /*
        GIFObj CRUD operations
     */
    public void saveGifObj(List<String> giphyIds, String categoryName, String userId) {
        GifCategory category = retrieveCategory(userId, categoryName);
        if(null == category) {
            category = new GifCategory(categoryName, userId, new ArrayList<GifObject>());
        }

        for (String giphyId : giphyIds) {
            category.getGifObjects().add(new GifObject(giphyId, categoryName, userId));
        }
        updateCategory(category);
    }


    public GifObject updateGifObject(String userId, String giphyId, String categoryName, String currentCatagoryName) {
        GifObject gifObj = new GifObject(giphyId, categoryName, userId);
        deleteGifObject(userId, giphyId, currentCatagoryName);
        GifCategory newCategory = retrieveCategory(userId, categoryName);
        if(null != newCategory) {
            newCategory.getGifObjects().add(gifObj);
        } else {
            newCategory = new GifCategory(categoryName, userId, Arrays.asList(gifObj));
        }
        updateCategory(newCategory);
        return gifObj;
    }

    public void deleteGifObject(String userId, String giphyId, String categoryName) {
        GifCategory category = retrieveCategory(userId, categoryName);
        category.getGifObjects().removeIf(gif -> gif.getGiphyId().equals(giphyId));
        updateCategory(category);
    }
}
