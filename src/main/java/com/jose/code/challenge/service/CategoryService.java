package com.jose.code.challenge.service;

import com.jose.code.challenge.data.DAOFacade;
import com.jose.code.challenge.data.ro.GifCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private DAOFacade dao;

    public List<GifCategory> getCategories(String userId) {
        List<GifCategory> categories = dao.retrieveCategories(userId);

        // TODO business logic? Maybe sort the list?

        return categories;
    }

    public void saveNewCategory(String categoryName, String userId) {
        GifCategory gifCategory = new GifCategory(categoryName, userId, Collections.emptyList());
        dao.createCategory(gifCategory);
    }

    public void deleteCategory(String categoryName, String userId) {
        dao.deleteCategory(categoryName); // TODO update this to query by a document of users not by category
    }

//    public void renameCategory(String categoryName, String updatedName) {
//        dao.updateCategory(categoryName, updatedName);
//
//    }
}
