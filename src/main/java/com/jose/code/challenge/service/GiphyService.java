package com.jose.code.challenge.service;

import com.jose.code.challenge.client.GiphyApiClient;
import com.jose.code.challenge.client.GiphyResources;
import com.jose.code.challenge.client.ro.GiphyResponseRO;
import com.jose.code.challenge.ro.exception.GiphyApiException;
import com.jose.code.challenge.ro.exception.GiphyApiParsingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GiphyService {

    @Autowired
    GiphyApiClient apiClient;

    public GiphyResponseRO queryGiphy(String query) throws GiphyApiException, GiphyApiParsingException {
        GiphyResponseRO responseRO = apiClient.invoke(query, GiphyResources.SEARCH);
        return responseRO;
    }
}
