package com.jose.code.challenge.service;

import com.jose.code.challenge.data.DAOFacade;
import com.jose.code.challenge.data.ro.GifObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class GifService {

    @Autowired
    private DAOFacade dao;

    public void saveGif(List<String> giphyIds, String categoryName, String userId) {
        categoryName = (null == categoryName || categoryName.isEmpty())? "uncategorized": categoryName;

        dao.saveGifObj(giphyIds, categoryName, userId);
    }

    // TODO accomplished via categories service
//    public List<GifObject> retrieveGifs(String userId) {
//        List<GifObject> gifs = dao.retrieveGifObjects(userId);
//
//        // TODO: business logic? Maybe sort?
//
//        return gifs;
//    }

    public void updateGifCategory(String userId, String giphyId, String categoryName, String currentCatagoryName) {
        categoryName = (null == categoryName || categoryName.isEmpty())? "uncategorized": categoryName;
        dao.updateGifObject(userId, giphyId, categoryName, currentCatagoryName);
    }

    public void deleteGif(String userId, String giphyId, String categoryName) {
        dao.deleteGifObject(userId, giphyId, categoryName);
    }
}
