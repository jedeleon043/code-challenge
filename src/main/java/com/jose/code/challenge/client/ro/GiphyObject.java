package com.jose.code.challenge.client.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GiphyObject implements Serializable {
    String type;
    String id;
    String slug;
    String url;
    @JsonProperty(value="bitly_url")
    String bitlyUrl;
    @JsonProperty(value="embed_url")
    String embedUrl;
    String username;
    String source;
    String rating;
    @JsonProperty(value="content_url")
    String contentUrl;
//    GiphyUser user;
    @JsonProperty(value="source_tld")
    String sourceTld;
    @JsonProperty(value="source_post_url")
    String sourcePostUrl;
    @JsonProperty(value="update_datetime")
    String updateDatetime;
    @JsonProperty(value="create_datetime")
    String createDatetime;
    @JsonProperty(value="import_datetime")
    String importDatetime;
    @JsonProperty(value="trending_datetime")
    String trendingDatetime;
    GiphyImages images;
    String title;
}
