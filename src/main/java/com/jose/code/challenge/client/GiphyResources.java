package com.jose.code.challenge.client;

public enum GiphyResources {
    SEARCH("/gifs/search");

    private String resource;

    private GiphyResources(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }
}
