package com.jose.code.challenge.client;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.jose.code.challenge.client.ro.GiphyResponseRO;
import com.jose.code.challenge.ro.exception.GiphyApiException;
import com.jose.code.challenge.ro.exception.GiphyApiParsingException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;

@Service
public class GiphyApiClient {

    @Value("${giphy.api.endpoint}")
    private String url;


    @Value("${giphy.api.key}")
    private String key;

    public GiphyResponseRO invoke(String query, GiphyResources resource) throws GiphyApiException, GiphyApiParsingException {
        try {
            Client client = ClientBuilder.newClient();
            Response response = client
                    .target(url)
                    .path(resource.getResource())
                    .queryParam("api_key", key)
                    .queryParam("q", query)
                    .queryParam("rating", "G")
                    .request()
                    .accept("image/*")
//                    .accept(MediaType.APPLICATION_JSON)
                    .get();

            if(null != response && response.getStatus() == 200) {
                Object entity = response.getEntity();
                MappingJsonFactory factory = new MappingJsonFactory();
                JsonParser parser = factory.createJsonParser((InputStream) entity);
                GiphyResponseRO giphyResponse = parser.readValueAs(GiphyResponseRO.class);
                return giphyResponse;
            } else {
                // TODO log and throw exception
                throw new GiphyApiException("Non 200 response invoking GiphyApi");
            }

        } catch (IOException e) {
            throw new GiphyApiParsingException(e.getMessage(), e);
        }
    }
}
