package com.jose.code.challenge.client.ro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class GiphyResponseRO  implements Serializable {
    List<GiphyObject> data;
    GiphyPaginationObject pagination;
    GiphyMetaObject meta;
}
