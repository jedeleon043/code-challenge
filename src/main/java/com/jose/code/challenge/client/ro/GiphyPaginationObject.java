package com.jose.code.challenge.client.ro;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GiphyPaginationObject implements Serializable {

    int offset;
    @JsonProperty(value = "total_count")
    int totalCount;
    int count;
}
