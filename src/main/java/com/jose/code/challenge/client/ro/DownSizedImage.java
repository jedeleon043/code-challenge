package com.jose.code.challenge.client.ro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownSizedImage implements Serializable {
    private String url;
    private String width;
    private String height;
    private String size;
}
