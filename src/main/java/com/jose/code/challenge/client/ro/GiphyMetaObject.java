package com.jose.code.challenge.client.ro;

import lombok.Data;

import java.io.Serializable;

@Data
public class GiphyMetaObject implements Serializable {
    int status;
    String msg;
    String response_id;
}
